# Tailwind

## Build
While developing, it is handy to run the following command:

```
npx tailwindcss -i ./src/input.css -o ./dist/output.css --watch
```

Once are building and running locally, run the following command to build the CSS isolated files:
```
npx postcss ./obj/Debug/net7.0/scopedcss/bundle/FFA.Client.styles.css -r  
```