﻿using FFA.Shared.Models;
using Microsoft.AspNetCore.Components;

namespace FFA.Client.Components;

public partial class ProfileCard
{
    [Parameter]
    public Profile Profile { get; set; } = new();
}
