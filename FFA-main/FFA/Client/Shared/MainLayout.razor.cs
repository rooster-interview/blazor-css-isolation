﻿using System;
using FFA.Shared.Models;
using MudBlazor;

namespace FFA.Client.Shared;

public partial class MainLayout
{
    // TODO: Load the user profile from identity
    private readonly Profile _profile = new()
    {
        Username = "t_listey",
        Age = 29,
        ProfilePhotoId = Guid.NewGuid()
    };

    private readonly MudTheme _currentTheme = new()
    {
        Palette = new PaletteDark()
        {
            Black = "#27282f",
            Background = "#32343d",
            BackgroundGrey = "#27282f",
            Surface = "#232526",
            TextPrimary = "#ffffffb2",
            TextSecondary = "rgba(255,255,255, 0.50)"
        }
    };
}
