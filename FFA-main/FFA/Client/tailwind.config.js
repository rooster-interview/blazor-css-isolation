/** @type {import('tailwindcss').Config} */
module.exports = {
    content: [
        './**/*.html',
        './**/*.css',
        './**/*.razor',
        './**/*.razor.css',
    ],
    theme: {
        colors: {
            'black': '#151718',
            'dark-gray': '#232526',
            'gray': '#343839',
            'green': '#3edd78',
            'blue': '#3d8ff0',
            'red': '#d74c0f',
            'purple': '#8e55ea',
        },
        extend: {},
    },
    plugins: [],
}
