﻿namespace FFA.Shared.Models;

public class Profile
{
    public Guid Id { get; set; }

    public string Username { get; set; } = string.Empty;

    public int Age { get; set; }

    public Guid ProfilePhotoId { get; set; }

    public IEnumerable<ProfilePhoto> ProfilePhotos { get; set; } = new List<ProfilePhoto>();
}

public class ProfilePhoto
{
    public Guid Id { get; set; }

    public string Base64String { get; set; } = string.Empty;
}
