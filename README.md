# Rooster Sample Blazor App



## Getting started

Please clone this repository and follow the instructions to complete your second round CSS Interview for Rooster.

1. Evaluate the current styles of the application and provide any style adjustments as you see fit. Treat the code like your own.
1. Isolate component styles to their own Blazor CSS Isolation file.
1. Remove CSS which is not necessary anymore.
1. Make a merge request.
